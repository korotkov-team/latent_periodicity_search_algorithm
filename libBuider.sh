#!/bin/bash

mkdir obj
cd ./obj
#gcc -I$1 -L$2 -c ../src/model.c ../src/error.c ../src/lpsa.c ../src/lpsa_symb_seq.c
gcc ../src/* -Dlinux -I../include -pthread -O3 -lm -c
cd ../bin
ar rc liblpsa.a ../obj/*.o
ranlib liblpsa.a
cd ../
rm -r ./obj
