# Алгоритм поиска скрытой периодичности

---

## Последовательности

* `lpsa_symbol`
	```c
	#define SYMBOL_MAX CHAR_MAX
	typedef unsigned char lpsa_symbol;
	```
Представляет собой символ. Не несёт семантической нагрузки, служит только некоторым обозначением. Запрещается использовать коды символов типа `char` в качестве значений этого типа. Следует использовать лишь числа из интервала $`[0,k]`$, где $`k`$ - количество различных символов в последовательности.

* `lpsa_symbolic_sequence`
	```c
	typedef struct {
		size_t       count_symbols;
		size_t       length;
		lpsa_symbol* symbols;
	} lpsa_symbolic_sequence;
	```
Представляет собой последовательность символов.  
`count_symbols` - количество различных символов в последовательности  
`length` - количество символов последовательности  
`symbols` - массив, содержащий непосредственно символы

---

## Обработка ошибок

**Сущности**

* `lpsa_error`
	```c
	typedef struct {
		unsigned int type;
		char*        message;
		lpsa_error*  wrapped;
	} lpsa_error;
	```
Структура, представляющая собой некоторую ошибку.  
`type` - тип ошибки  
`message` - сообщение, содержащее подробную информацию об ошибке  
`wrapped` - другая ошибка, которая обёрнута в текущую  
Механизм оборачивания ошибок введён для того, чтобы корректней отражать стек их возникновения.

**Константы**
* `LPSA_NOT_ENOUGH_MEMORY` возвращается в случае, если аллоцирующие память функции не смогли этого сделать.
* `LPSA_WRONG_ARGS` - возвращается в случае, когда аргументы, переданные в функцию не удовлетворяют некоторым ограничениям.
* `LPSA_STACK_IS_EMPTY` - специальная ошибка, возвращающаяся только функциями, связанными с `lpsa_stack`, сигнализирущая о том, что стек пуст и из него нельзя больше брать значений.
* `LPSA_WRAPPED_ERROR_TYPE` - возвращается в случае, если данная ошибка оборачивает другую ошибку. Это необязательное требование, но так стоит делать, если нельзя указать другую семантику.

**Методы**

* `lpsa_error_new`
	```c
	lpsa_error* lpsa_error_new(
	unsigned int type, 
	char*        msg)
	```
Конструктор ошибки.

* `lpsa_error_print`
	```c
	void lpsa_error_print(const lpsa_error* err)
	```
Выводит сообщение об ошибке на консоль при помощи функции `printf`.

* `lpsa_error_println`
	```c
	void lpsa_error_println(const lpsa_error* err)
	```
Выводит сообщение об ошибке на консоль при помощи функции `printf` и переводит курсор на новую строку.

* `lpsa_error_wrap`
	```c
	lpsa_error* lpsa_error_wrap(
		lpsa_error*  err, 
		unsigned int type, 
		char*        msg)
	```
Создаёт новый экземпляр `lpsa_error`, но в поле `wrapped` добавляет `err`.

---

## Части алгоритма

* `create_artificial_sequence`
	```c
	lpsa_symbolic_sequence* create_artificial_sequence(
		const lpsa_model* model, 
		size_t            seq_length)
	```
Создаёт искусственную периодическую последовательность вида $`[1,...,n,1,...,n,...]`$, где $`n`$ это длина исследуемого периода.

* `generate_random_sequence_from_existed`
	```c
	lpsa_symbolic_sequence* generate_random_sequence_from_existed(
		const lpsa_symbolic_sequence* seq)
	```
Перемешивает символы последовательности `seq` и возвращает новую последовательность.

* `generate_random_sequence_from_existed_blocks`
	```c
	lpsa_symbolic_sequence* generate_random_sequence_from_existed_blocks(
		const lpsa_symbolic_sequence* seq, 
		const size_t                  block)
	```
Перемешивает не отдельные символы, а блоки последовательности `seq`. Длину блока представляет параметр `block`.

* `distance`
	```c
	lpsa_error* distance(
		const gsl_matrix* m1, 
		const gsl_matrix* m2, 
		double*           out)
	```
Вычисляет расстояние между двумя матрицами в евклидовом пространстве.

* `create_frequency_matrix`
	```c
	lpsa_error* create_frequency_matrix(
		const lpsa_symbolic_sequence* artificial, 
		const lpsa_symbolic_sequence* random, 
		gsl_matrix*                   out)
	```
Создаёт частотную матрицу, отражающую, сколько раз встретился конкретный символ на конкретной позиции периода. Для этого используется совместное сравнение искусственной матрицы, сгенерированной методом `create_artificial_sequence` и целевой `random`. Результат возвращается в параметре `out`.

* `create_weight_matrix`
	```c
	lpsa_error* create_weight_matrix(
		const gsl_matrix* v, 
		gsl_matrix*       m)
	```
Создаёт позиционно-весовую матрицу `m`, исходя из частотной матрицы `v`, предварительно полученной с помощью функции `create_frequency_matrix`.

* `calculate_m_prime`
	```c
	lpsa_error* calculate_m_prime(
		const gsl_matrix*             m, 
		const lpsa_symbolic_sequence* S, 
		const double                  R, 
		const double                  K_d, 
		const size_t                  period_length, 
		gsl_matrix*                   out)
	```
Вычисляет матрицу $`M'`$ по алгоритму, описанному в статье. `m` - позиционно-весовая матрица, полученная с помощью функции `create_weight_matrix`.

* `create_matrix_set`
	```c
	lpsa_error* create_matrix_set(
		const lpsa_model*             model, 
		const lpsa_symbolic_sequence* seq, 
		const lpsa_symbolic_sequence* artificial, 
		gsl_matrix**                  set)

	```
Создаёт множество случайных последовательностей $`\{Sr\}`$, затем сразу же вычисляет на их основе позиционно-весовые матрицы $`M`$ и формирует множество $`Q_n`$, где все эти матрицы отстают друг от друга не менее, чем на $`D_0`$.  
Размер множества регулируется параметром `model->size_of_matrix_set`  
Параметр $`D_0`$ регулируется параметром `model->min_distance_between_matrices`

* `align_sequence_with_ideal`
	```c
	void align_sequence_with_ideal(
		const lpsa_symbolic_sequence* S, 
		const lpsa_symbolic_sequence* S1, 
		const gsl_matrix*             Mq, 
		const lpsa_model*             model, 
		unsigned char                 return_alignment,
		lpsa_symbolic_sequence*       S_alignment, 
		lpsa_symbolic_sequence*       S1_alignment, 
		double*                       F_max)
	```
Производит локальное выравнивание некоторой последовательности $`S`$ и искусственной последовательности $`S_1`$. Дополнительно для вычисления матриц выравнивания необходима матрица $`M'`$ (`Mq`) и значения штрафов за пропуск `model->d_price` и за повторный пропуск `model->e_price`.
Параметр `return_alignment` - это флаг, определяющий необходимость построения выравнивания последовательностей в явном виде. Если это `0`, то в качестве параметров `S_alignment` и `S1_alignment` можно послать `NULL`.
Параметр `F_max` обозначает вес получившегося локального выравнивания.