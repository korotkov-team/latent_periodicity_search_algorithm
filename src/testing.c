#include "../include/lpsa.h"
#include "../include/matrix.h"
#include "../include/thread_pool.h"
#include <stdio.h>
#include <time.h>

/*-----------------------------------------------------------------------------
 * Testing the algorithm
 */

void test_create_artificial_sequence() {
	printf("\nTESTING FUNC: %s\n", "create_artificial_sequence");
	size_t test_number = 3;
	size_t test_options[3] = { 40, 138, 596000 };
	lpsa_model model;
	use_default_model(&model);
	for (size_t i = 0; i < test_number; ++i) {
		size_t seq_length = test_options[i];
		char pass = 1;
		printf("\tTEST %zd: seq_length = %zd\n", i + 1, seq_length);
		lpsa_symbolic_sequence* seq = create_artificial_sequence(&model, seq_length);
		for (size_t j = 0; j < seq_length; ++j) {
			if (seq->symbols[j] != j % model.period_length) {
				printf("\tERROR: artificial sequence is not right. j=%zd, period=%zd, symbols[j]=%d <> j%%period=%zd\n", 
					j, model.period_length, seq->symbols[j], j % model.period_length);
				pass = 0;
				break;
			}
		}
		free(seq->symbols);
		free(seq);
		if (pass == 1) {
			printf("\tPASSED\n");
		}
	}
}

void test_generate_random_sequence_from_existed() {
	printf("\nTESTING FUNC: %s\n", "generate_random_sequence_from_existed");
	lpsa_symbolic_sequence* seq = calloc(sizeof(lpsa_symbolic_sequence), 1);
	seq->count_symbols = 4;
	seq->length = 10;
	seq->symbols = calloc(sizeof(lpsa_symbol), seq->length);
	printf("\tINITIAL: ");
	for (lpsa_symbol i = 0; i < seq->length; ++i) {
		seq->symbols[i] = i % 4;
		printf("%d ", seq->symbols[i]);
	}
	printf("\n");
	lpsa_symbolic_sequence* result = generate_random_sequence_from_existed(seq);
	printf("\tRESULT: ");
	for (size_t i = 0; i < result->length; ++i) {
		printf("%d ", result->symbols[i]);
	}
	printf("\n");
	lpsa_error* err = lpsa_symbolic_sequence_is_consistent(seq, result);
	if (err != NULL) {
		printf("\tFAILED: %s\n", err->message);
	}
	else {
		printf("\tPASSED\n");
	}
}

void test_generate_random_sequence_from_existed_blocks() {
	printf("\nTESTING FUNC: %s\n", "generate_random_sequence_from_existed_blocks");
	lpsa_symbolic_sequence* seq = calloc(sizeof(lpsa_symbolic_sequence), 1);
	seq->count_symbols = 4;
	seq->length = 10;
	seq->symbols = calloc(sizeof(lpsa_symbol), seq->length);
	printf("\tINITIAL: ");
	for (lpsa_symbol i = 0; i < seq->length; ++i) {
		seq->symbols[i] = i % 4;
		printf("%d ", seq->symbols[i]);
	}
	printf("\n");
	lpsa_symbolic_sequence* result = generate_random_sequence_from_existed_blocks(seq, 3);
	printf("\tRESULT: ");
	for (size_t i = 0; i < result->length; ++i) {
		printf("%d ", result->symbols[i]);
	}
	printf("\n");
	lpsa_error* err = lpsa_symbolic_sequence_is_consistent(seq, result);
	if (err != NULL) {
		printf("\tFAILED: %s\n", err->message);
	}
	else {
		printf("\tPASSED\n");
	}
}

lpsa_matrix* test_create_frequency_matrix() {
	printf("\nTESTING FUNC: create_frequency_matrix\n");
	lpsa_symbol S[21] =   { 0,3,2,2,3,1,3,0,1,0,2,3,1,2,3,0,0,0,3,0,2 };
	lpsa_symbol S_1[21] = { 0,1,2,3,4,0,1,2,3,4,0,1,2,3,4,0,1,2,3,4,0 };
	double freq[4][5] = {
		2,1,2,0,2,
		1,0,1,1,0,
		2,0,1,2,0,
		0,3,0,1,2
	};
	lpsa_symbolic_sequence artificial;
	artificial.count_symbols = 5;
	artificial.length = 21;
	artificial.symbols = S_1;
	lpsa_symbolic_sequence random;
	random.count_symbols = 4;
	random.length = 21;
	random.symbols = S;
	lpsa_matrix* result = lpsa_matrix_init(4, 5);
	lpsa_error* err = create_frequency_matrix(&artificial, &random, result);
	if (err != NULL) {
		printf("\tERROR: internal: %s", err->message);
		free(err);
		return NULL;
	}
	free(err);
	char flag = 0;
	if (result->lines != 4) {
		printf("\tERROR: lines number is not right. have=%zd expected=%d\n", result->lines, 4);
		flag = 1;
	}
	if (result->columns != 5) {
		printf("\tERROR: columns number is not right. have=%zd expected=%d\n", result->columns, 5);
		flag = 1;
	}
	if (flag != 0) {
		return NULL;
	}
	flag = 0;
	printf("\thave:expected\n");
	for (int i = 0; i < 4; ++i) {
		printf("\t");
		for (int j = 0; j < 5; ++j) {
			printf("%f:%f ", lpsa_matrix_get(result, i, j), freq[i][j]);
			if (lpsa_matrix_get(result, i, j) != freq[i][j]) {
				flag = 1;
			}
		}
		printf("\n");
	}
	if (flag != 0) {
		printf("\tERROR: frequency matrix have incorrect values\n");
	}
	else {
		printf("\tPASSED\n");
	}
	return result;
}

lpsa_matrix* test_create_weight_matrix(lpsa_matrix* freq) {
	printf("\nTESTING FUNC: create_weight_matrix\n");
	lpsa_matrix* m = lpsa_matrix_init(freq->lines, freq->columns);
	lpsa_error* err = create_weight_matrix(freq, m);
	if (err != NULL) {
		printf("\tERROR: %s\n", err->message);
	}
	for (size_t i = 0; i < m->lines; ++i) {
		printf("\t");
		for (size_t j = 0; j < m->columns; ++j) {
			printf("%f ", lpsa_matrix_get(m, i, j));
		}
		printf("\n");
	}
	printf("\tPASSED\n");
	return m;
}

void test_lpsa_package() {
	printf("\nStarting tests for LPSA\n");
	test_generate_random_sequence_from_existed();
	test_generate_random_sequence_from_existed_blocks();
	test_create_artificial_sequence();
	lpsa_matrix* freq = test_create_frequency_matrix();
	test_create_weight_matrix(freq);
	printf("\nAll tests done\n");
}

/*-----------------------------------------------------------------------------
 * Testing the matrix
 */

lpsa_matrix* test_matrix_init() {
	printf("\nTESTING matrix_init\n");
	double data[5][6] = {
		1,2,3,4,5,6,
		5,4,3,2,1,0,
		0,1,0,1,0,1,
		1,0,1,0,1,0,
		9,9,9,9,9,9
	};
	lpsa_matrix* matrix = lpsa_matrix_init(5, 6);
	if (matrix->lines != 5) {
		printf("\tERROR: wrong count of lines in matrix: have %zd, want 5\n", matrix->lines);
		return NULL;
	}
	if (matrix->columns != 6) {
		printf("\tERROR: wrong count of columns in matrix: have %zd, want 6\n", matrix->columns);
		return NULL;
	}
	for (size_t i = 0; i < matrix->lines; ++i) {
		for (size_t j = 0; j < matrix->columns; ++j) {
			lpsa_matrix_set(matrix, i, j, data[i][j]); // check allocation somehow
		}
	}
	printf("\tPASSED\n");
	return matrix;
}

void test_matrix_get_set(lpsa_matrix* matrix) {
	printf("\nTESTING matrix_get and matrix_set\n");
	lpsa_matrix_set(matrix, 3, 4, 15);
	if (lpsa_matrix_get(matrix, 3, 4) != 15) {
		printf("\tERROR: get/set working wrong: have %f, want 15\n", lpsa_matrix_get(matrix, 3, 4));
		return;
	}
	printf("\tPASSED\n");
}

void test_matrix_max(lpsa_matrix* matrix) {
	printf("\nTESTING matrix_max\n");
	double max = 0;
	size_t max_line = 0;
	size_t max_column = 0;
	lpsa_matrix_max(matrix, &max, &max_line, &max_column);
	if (max != 15 && max_line != 3 && max_column != 4) {
		printf("\tERROR: wrong max func: have max=%f, line=%zd, column=%zd, want max=15, line=3, column=4\n", max, max_line, max_column);
		return;
	}
	printf("\tPASSED\n");
}

void test_matrix_set_all(lpsa_matrix* matrix) {
	printf("\nTESTING matrix_set_all\n");
	double value = 0;
	lpsa_matrix_set_all(matrix, value);
	for (size_t i = 0; i < matrix->lines; ++i) {
		for (size_t j = 0; j < matrix->columns; ++j) {
			if (lpsa_matrix_get(matrix, i, j) != value) {
				printf("\tERROR: wrong value in set all: have %f, want %f\n", lpsa_matrix_get(matrix, i, j), value);
				return;
			}
		}
	}
	printf("\tPASSED\n");
}

void test_matrix_package() {
	printf("\nStarting tests for lpsa_matrix\n");
	lpsa_matrix* matrix = test_matrix_init();
	test_matrix_get_set(matrix);
	test_matrix_max(matrix);
	test_matrix_set_all(matrix);
	printf("\nAll tests done\n");
}

/*-----------------------------------------------------------------------------
 * Testing the algorithm
 */

void insert_symbol(lpsa_symbolic_sequence* seq, lpsa_symbol val, size_t index){ //without reallocating. Must be used complimentary with delete_symbol (deleting first)
	for (size_t i = seq->length - 2; i >= index; --i){
		seq->symbols[i + 1] = seq->symbols[i];
	}
	seq->symbols[index] = val;
}

void delete_symbol(lpsa_symbolic_sequence* seq, size_t index) { //without reallocating. Must be used complimentary with insert_symbol
	for (size_t i = index; i < seq->length - 1; ++i) {
		seq->symbols[i] = seq->symbols[i+1];
	}
}

lpsa_symbolic_sequence** create_sequences_set(
	const lpsa_model* model, 
	size_t period_length,
	size_t periods_number,
	size_t experiments_n,
	size_t* rnd_interchanges_numb,
	size_t def_interchanges_numb
){
	lpsa_symbolic_sequence** distorted_seqs = (lpsa_symbolic_sequence**)malloc(experiments_n*sizeof(lpsa_symbolic_sequence*));
	for (size_t i = 0; i < experiments_n; ++i){
		distorted_seqs[i] = (lpsa_symbolic_sequence*)malloc(sizeof(lpsa_symbolic_sequence));
		distorted_seqs[i]->count_symbols = model->size_of_symbols_set;
		distorted_seqs[i]->length = period_length * periods_number;
		distorted_seqs[i]->symbols = (lpsa_symbol*)malloc(distorted_seqs[i]->length * sizeof(lpsa_symbol));
	}

	//creating periodic sequences for each experiment without deletions and assertions
	srand((unsigned int)time(NULL));
	for (size_t i = 0; i < period_length; i++){
		lpsa_symbol rand_sym = rand() % model->size_of_symbols_set;
		for (size_t j = 0; j < periods_number; ++j){
			for (size_t k = 0; k < experiments_n; ++k){
				distorted_seqs[k]->symbols[j*period_length + i] = rand_sym;
			}
		}
	}

	//adding rnd interchanges
	for (size_t exp = 0; exp < experiments_n; ++exp){
		for (size_t i = 0; i < rnd_interchanges_numb[exp]; ++i){
			distorted_seqs[exp]->symbols[rand() % (period_length * periods_number)] = (lpsa_symbol)(rand() % model->size_of_symbols_set);
		}
	}
	
	//adding defined deletions and assertions

	//deletions
	lpsa_symbol del_sym = rand() % model->size_of_symbols_set;
	for (size_t i = 0; i < def_interchanges_numb; i++){
		size_t pos = rand() % (period_length * periods_number);
		for (size_t exp = 0; exp < experiments_n; ++exp){
			size_t local_pos = pos;
			while (distorted_seqs[exp]->symbols[local_pos] != del_sym){
				if (local_pos == distorted_seqs[exp]->length-1){
					local_pos = 0;
				}
				else {
					++local_pos;
				}
			}
			delete_symbol(distorted_seqs[exp], local_pos);
		}
	}

	//insertions
	lpsa_symbol ins_sym = rand() % model->size_of_symbols_set;
	for (size_t i = 0; i < def_interchanges_numb; i++){
		size_t pos = rand() % (period_length * periods_number);
		for (size_t exp = 0; exp < experiments_n; ++exp){
			insert_symbol(distorted_seqs[exp], ins_sym, pos);
		}
	}
	return distorted_seqs;
}

void test_sequence_alignment_single_thread(
	lpsa_model* model,
	size_t experiments_n,
	lpsa_symbolic_sequence** distorted_seqs
){
	double* F_max = (double*)malloc(sizeof(double));
	double* significance = (double*)malloc(sizeof(double));
	lpsa_error* err = NULL;
	for (size_t exp = 0; exp < experiments_n; ++exp){
		printf("Testing for sequence %ld:\n\r", exp);
		for (size_t p = model->spectre_min; p <= model->spectre_max; ++p){
			model->period_length = p;
			//lpsa_symbolic_sequence* S = calloc(sizeof(lpsa_symbolic_sequence), 1);
			//lpsa_symbolic_sequence* S1 = calloc(sizeof(lpsa_symbolic_sequence), 1);
			time_t elapsed = time(NULL);
			err = lpsa_algorithm(model, distorted_seqs[exp], NULL, NULL, F_max, significance, LPSA_ALIGN_TYPE_GLOBAL);
			if (err != NULL){
				lpsa_error_println(err);
				free(err);
			}
			elapsed = time(NULL) - elapsed;
			printf("For period %ld: F = %.5lf and Z = %.5lf. Elapsed time: %ld sec\n", p, *F_max, *significance, elapsed);
		}
		printf("_________________________________________________\n\r");
	}
	//ridding memory
	free(F_max);
	free(significance);
}


void test_sequence_alignment_multiple_thread(
	lpsa_model* model,
	lpsa_thread_pool* pool,
	size_t experiments_n,
	lpsa_symbolic_sequence** distorted_seqs
){
	lpsa_conc_query* query = lpsa_conc_query_init();
	lpsa_functor* functor = calloc(sizeof(lpsa_functor), 1);
	*functor = lpsa_algorithm_async;
	for (size_t exp = 0; exp < experiments_n; ++exp){
		printf("Testing for sequence %ld:\n\r", exp);
		for (size_t p = model->spectre_min; p <= model->spectre_max; ++p){
			lpsa_model* tmodel = calloc(sizeof(lpsa_model), 1);
			*tmodel = *model;
			tmodel->period_length = p;
			double* F_max = calloc(sizeof(double), 1);
			double* Z = calloc(sizeof(double), 1);
			lpsa_algorithm_args* args = calloc(sizeof(lpsa_algorithm_args), 1);
			args->alignment_type = LPSA_ALIGN_TYPE_GLOBAL;
			args->F_max = F_max;
			args->model = tmodel;
			args->S = distorted_seqs[exp];
			args->S1_alignment = NULL;
			args->S_alignment = NULL;
			args->significance = Z;
			lpsa_task_args* task_args = calloc(sizeof(lpsa_task_args), 1);
			task_args->args = args;
			task_args->container = query;
			lpsa_task* task = calloc(sizeof(lpsa_task), 1);
			task->function = functor;
			task->arg = task_args;
			lpsa_thread_pool_add(pool, task);
		}
		size_t results_count = 0;
		for (;;) {
			if (results_count == model->spectre_max - model->spectre_min + 1) {
				break;
			}
			if (query->count != 0) {
				lpsa_algorithm_result result = *(lpsa_algorithm_result*)lpsa_conc_query_pop(query);
				printf("Period %ld: Z = %.3lf. Elapsed time: %ld sec.\n", result.period, *result.significance, *result.elapsed_time);
				results_count++;
			}
			wait;
		}
	}
}


void test_alignment_single_thread_package(){
	lpsa_model* model = (lpsa_model*)malloc(sizeof(lpsa_model));
	model->d_price = 16;
	model->e_price = 4;
	model->size_of_matrix_set = 50;
	model->size_of_symbols_set = 4;
	model->min_distance_between_matrices = 1 * (model->size_of_symbols_set * model->size_of_symbols_set);
	model->spectre_min = 4;
	model->spectre_max = 100;
	model->threads_num = 1;
	model->period_length = model->spectre_min;

	size_t experiments_n = 5;
	size_t interchanges[5] = { 500, 1000, 2000, 3000, 4000 };

	lpsa_symbolic_sequence** experimental_sequences = create_sequences_set(model, 40, 50, experiments_n, interchanges, 5);
	printf("Experimental sequences generated\n\r________________________________________________________________\n\r");
	test_sequence_alignment_single_thread(model, experiments_n, experimental_sequences);
	printf("\n\r__Allignment tests finished__\n\r");
	//ridding memory
	for (size_t exp = 0; exp < experiments_n; ++exp){
		free(experimental_sequences[exp]->symbols);
		free(experimental_sequences[exp]);
	}
	free(experimental_sequences);
	free(model);
}

void test_alignment_multiple_threads_package(){
	lpsa_model* model = (lpsa_model*)malloc(sizeof(lpsa_model));
	model->d_price = 16;
	model->e_price = 4;
	model->size_of_matrix_set = 50;
	model->size_of_symbols_set = 4;
	model->min_distance_between_matrices = 1 * (model->size_of_symbols_set * model->size_of_symbols_set);
	model->spectre_min = 4;
	model->spectre_max = 100;
	model->threads_num = 4;
	model->period_length = model->spectre_min;

	size_t experiments_n = 5;
	size_t interchanges[5] = { 500, 1000, 2000, 3000, 4000 };

	lpsa_symbolic_sequence** experimental_sequences = create_sequences_set(model, 40, 50, experiments_n, interchanges, 5);
	printf("Experimental sequences generated\n\r________________________________________________________________\n\r");
	lpsa_thread_pool* thread_pool = lpsa_thread_pool_init(model->threads_num);
	test_sequence_alignment_multiple_thread(model, thread_pool, experiments_n, experimental_sequences);
	printf("\n\r__Allignment tests finished__\n\r");
	//ridding memory
	for (size_t exp = 0; exp < experiments_n; ++exp){
		free(experimental_sequences[exp]->symbols);
		free(experimental_sequences[exp]);
	}
	free(experimental_sequences);
	free(model);
	//...

}

/*-----------------------------------------------------------------------------
 * Testing the thread pool
 */

void* task_common(void* value) {
	printf("hello I am thread\n");
	lpsa_task_args* args = (lpsa_task_args*)value;
	size_t i = *(size_t*)args->args;
	size_t* _i = malloc(sizeof(size_t));
	*_i = i;
	lpsa_conc_query_push(args->container, _i);
	return NULL;
}

void test_THREAD_POOL_common() {
	size_t count = 20;
	lpsa_thread_pool* thread_pool = lpsa_thread_pool_init(4);
	lpsa_functor* f = calloc(sizeof(lpsa_functor), 1);
	*f = task_common;
	lpsa_conc_query* query = lpsa_conc_query_init();
	for (size_t i = 0; i < count; ++i) {
		size_t* _i = calloc(sizeof(size_t), 1);
		*_i = i*2;
		lpsa_task_args* args = calloc(sizeof(lpsa_task_args), 1);
		args->args = _i;
		args->container = query;
		lpsa_task* task = calloc(sizeof(lpsa_task), 1);
		task->arg = args;
		task->function = f;
		lpsa_thread_pool_add(thread_pool, task);
	}
	for (;;) {
		if (query->count == count) {
			break;
		}
		wait;
		//sleep(1);
		//printf("%d\n", query->count);
	}
	for (size_t i = 0; i < count; ++i) {
		printf("%ld ", *(size_t*)lpsa_conc_query_pop(query));
	}
}

void test_THREAD_POOL_package() {
	test_THREAD_POOL_common();
}


void test_alignment_multiple_threads_package_(){
	lpsa_model* model = (lpsa_model*)malloc(sizeof(lpsa_model));
	model->d_price = 16;
	model->e_price = 4;
	model->size_of_matrix_set = 100;
	model->size_of_symbols_set = 20; // for proteins
	model->min_distance_between_matrices = 1 * (model->size_of_symbols_set* model->size_of_symbols_set);
	model->spectre_min = 4;
	model->spectre_max = 100;
	model->threads_num = 4;
	model->period_length = model->spectre_min;

	size_t experiments_n = 5;
	size_t interchanges[5] = { 500, 1000, 2000, 3000, 4000 };

	lpsa_symbolic_sequence** experimental_sequences = create_sequences_set(model, 40, 50, experiments_n, interchanges, 50);
	printf("Experimental sequences generated\n\r________________________________________________________________\n\r");
	
	lpsa_thread_pool* thread_pool = lpsa_thread_pool_init(model->threads_num);

	double* F_max = (double*)malloc(sizeof(double));
	double* significance = (double*)malloc(sizeof(double));
	lpsa_error* err = NULL;

	for (size_t exp = 0; exp < experiments_n; ++exp){
		printf("Testing for sequence %ld:\n\r", exp);
		for (size_t p = model->spectre_min; p <= model->spectre_max; ++p){
			model->period_length = p;
			time_t elapsed = time(NULL);
			err = lpsa_algorithm_multiple_threads(model, experimental_sequences[exp], NULL, NULL, F_max, significance, LPSA_ALIGN_TYPE_GLOBAL, thread_pool);
			if (err != NULL){
				lpsa_error_println(err);
				free(err);
			}
			elapsed = time(NULL) - elapsed;
			printf("\tFor period %ld: F = %.5lf and Z = %.5lf. Elapsed time: %ld sec\n\r", p, *F_max, *significance, elapsed);
		}
		printf("_________________________________________________\n\r");
	}

	printf("\n\r__Allignment tests finished__\n\r");
	//ridding memory
	free(F_max);
	free(significance);
	for (size_t exp = 0; exp < experiments_n; ++exp){
		free(experimental_sequences[exp]->symbols);
		free(experimental_sequences[exp]);
	}
	free(experimental_sequences);
	free(model);
	lpsa_thread_pool_stop(thread_pool); //bad way
	lpsa_thread_pool_free(thread_pool);
}

#include <math.h>

void test_alignment_with_normalization() {
	double min_dist = 7;
	lpsa_model* model = (lpsa_model*)malloc(sizeof(lpsa_model));
	model->d_price = 20;
	model->e_price = 5;
	model->size_of_matrix_set = 50;
	model->size_of_symbols_set = 20;
	model->min_distance_between_matrices = min_dist;
	model->spectre_min = 4;
	model->spectre_max = 100;
	model->threads_num = 4;
	model->period_length = model->spectre_min;
	model->K_d = -0.1;

	size_t experiments_n = 5;
	size_t interchanges[5] = { 500, 1000, 2000, 3000, 4000 };
	char* files[5] = { "test_spectrum500.txt", "test_spectrum1000.txt", "test_spectrum2000.txt" , "test_spectrum3000.txt", "test_spectrum4000.txt" };

	lpsa_symbolic_sequence** experimental_sequences = create_sequences_set(model, 40, 50, experiments_n, interchanges, 5);

	size_t start_period = 3;
	size_t end_period = 61;
	size_t total_periods = end_period - start_period;

	for (size_t exp = 0; exp < experiments_n; ++exp) {
		double* F_max = (double*)malloc(sizeof(double));
		double* significance = (double*)malloc(sizeof(double));
		double* spectrum = calloc(sizeof(double), total_periods);
		for (size_t p = start_period; p < end_period; ++p) {
			model->period_length = p;
			model->min_distance_between_matrices = 7*tanh(p*p / 18.0);
			double sxi = 1050;
			model->R_sqr = (sxi / pow(7, 0.61))*(pow(model->period_length, 0.61));
			time_t elapsed = time(NULL);
			lpsa_error* err = lpsa_start_with_normalization(model, experimental_sequences[exp], NULL, NULL, significance);
			if (err != NULL) {
				lpsa_error_println(err);
				free(err);
			}
			elapsed = time(NULL) - elapsed;
			printf("Elapsed time: %zd sec\n", elapsed);
			spectrum[p - start_period] = *significance;
		}
		FILE* file = fopen(files[exp], "w");
		for (size_t i = 0; i < total_periods; ++i) {
			char buffer[255];
			sprintf(buffer, "%.3lf\n", spectrum[i]);
			fputs(buffer, file);
		}
		fclose(file);
	}
}

/*
If you want to start tests just rename this function to `main` and build sources as an application. (lpsa_test)
*/
void lpsa_test() {
	//test_matrix_package();
	//test_lpsa_package();
	//test_alignment_single_thread_package();
	//test_THREAD_POOL_package();
	//test_alignment_multiple_threads_package();
	test_alignment_with_normalization();
	getchar();
	//return 0;
}