#include "../include/lpsa.h"
#include <string.h>
#include <math.h>
#include "../include/thread_pool.h"
#include <stdio.h>

#define lpsa_max(a,b) (((a) > (b)) ? (a) : (b))

unsigned char*** make_uchar_matrix3(
	size_t m, 
	size_t n)
{
	unsigned char*** F_ = (unsigned char***)calloc(3, sizeof(unsigned char**));
	for (size_t i = 0; i < 3; ++i) {
		F_[i] = (unsigned char**)calloc(m, sizeof(unsigned char*));
		for (size_t j = 0; j < m; ++j) {
			F_[i][j] = (unsigned char*)calloc(n, sizeof(unsigned char));
		}
	}
	return F_;
}

lpsa_symbolic_sequence* generate_random_sequence_from_existed(
	const lpsa_symbolic_sequence* seq) 
{
	// �������������. O(1)
	lpsa_symbolic_sequence* result = (lpsa_symbolic_sequence*)malloc(sizeof(lpsa_symbolic_sequence) * seq->length);
	result->length = seq->length;
	result->count_symbols = seq->count_symbols;
	result->symbols = malloc(sizeof(lpsa_symbol) * seq->length);
	memcpy(result->symbols, seq->symbols, sizeof(lpsa_symbol) * seq->length);
	// �� ������ ���� �������� ���������� `new_index` � ����� ������� �� �������� ������� � �� ����� �������. O(n)
	for (unsigned int i = 0; i < result->length; ++i) {
		int new_index = rand() % result->length;
		lpsa_symbol temp = result->symbols[i];
		result->symbols[i] = result->symbols[new_index];
		result->symbols[new_index] = temp;
	}
	return result;
	// ���������: O(n)
}

lpsa_symbolic_sequence* generate_random_sequence_from_existed_blocks(
	const lpsa_symbolic_sequence* seq, 
	const size_t                  block) 
{
	// Creation of new symbolic_sequence. O(1)
	lpsa_symbolic_sequence* result = (lpsa_symbolic_sequence*)malloc(sizeof(lpsa_symbolic_sequence) * seq->length);
	result->length = seq->length;
	result->count_symbols = seq->count_symbols;
	result->symbols = malloc(sizeof(lpsa_symbol) * seq->length);
	memcpy(result->symbols, seq->symbols, sizeof(lpsa_symbol) * seq->length);
	// It guarantees that `length - block_count > block`.
	size_t blocks_count = result->length / block;
	// For each step it randomly defines `new_index` of block and then swaps current and selected blocks. O(n)
	for (size_t i = 0; i < blocks_count; ++i) {
		size_t new_index = rand() % blocks_count;
		lpsa_symbol* temp = malloc(sizeof(lpsa_symbol)*block);
		size_t i_b = i * block;
		size_t new_index_b = new_index * block;
		memcpy(temp, result->symbols + i_b, sizeof(lpsa_symbol)*block);
		memcpy(result->symbols + i_b, result->symbols + new_index_b, sizeof(lpsa_symbol)*block);
		memcpy(result->symbols + new_index_b, temp, sizeof(lpsa_symbol)*block);
		free(temp);
	}
	return result;
	// Resulting: O(n)
	// Optimization: unknown
}

// �������� ������������� ������������������
lpsa_symbolic_sequence* create_artificial_sequence(
	const lpsa_model* model,     // ����� �������
	size_t            seq_length // ����� ������������������
) {
	// �������������. O(1)
	lpsa_symbolic_sequence* result = malloc(sizeof(lpsa_symbolic_sequence));
	result->count_symbols = model->period_length;
	result->length = seq_length;
	result->symbols = malloc(sizeof(lpsa_symbol) * seq_length);
	// ���������� ������������������ ���������. O(n)
	for (size_t i = 0; i < seq_length; ++i) {
		result->symbols[i] = i % result->count_symbols;
	}
	return result;
	// ���������: O(n)
}

// �������� ��������� �������
lpsa_error* create_frequency_matrix(
	const lpsa_symbolic_sequence* artificial, // ������������� ������������������
	const lpsa_symbolic_sequence* random,     // ������������ ������������������
	OUT lpsa_matrix*              V           // ��������� �������
) {
	if (artificial->length != random->length) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "length of artifitical and random sequences are not equal");
	}
	if (V->lines != random->count_symbols) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "number of lines in frequency matrix is not equal to symbols count of random sequence");
	}
	if (V->columns != artificial->count_symbols) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "number of columns in frequency matrix is not equal to symbols count of artificial sequence");
	}
	// �������������. O(symbols_count * period_length) = O(1)
	lpsa_matrix_set_all(V, 0);
	// �������� ��������� �������. O(n)
	for (size_t i = 0; i < artificial->length; ++i) {
		size_t line = random->symbols[i];
		size_t column = artificial->symbols[i];
		double item = lpsa_matrix_get(V, line, column);
		lpsa_matrix_set(V, random->symbols[i], artificial->symbols[i], item + 1);
	}
	return NULL;
	// ���������: O(n)
}

// �������� ����������-������� �������
lpsa_error* create_weight_matrix(
	const lpsa_matrix* v, // ��������� �������
	OUT lpsa_matrix*   m  // �������������� �������
) {
	if (v->lines != m->lines) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "count of lines in matrices V and M is not equal");
	}
	if (v->columns != m->columns) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "count of columns in matrices V and M is not equal");
	}
	// �������������. O(lines*columns)
	lpsa_matrix_set_all(m, 0);
	double N = 0;
	double* x = calloc(sizeof(double), v->lines);
	double* y = calloc(sizeof(double), v->columns);
	// ���������� ��������������� �������� x, y, � ���������� ���������� �������� N. O(lines*columns)
	for (size_t i = 0; i < v->lines; ++i) {
		for (size_t j = 0; j < v->columns; ++j) {
			double item = lpsa_matrix_get(v, i, j);
			N += item;
			x[i] += item;
		}
	}
	for (size_t j = 0; j < v->columns; ++j) {
		for (size_t i = 0; i < v->lines; ++i) {
			double item = lpsa_matrix_get(v, i, j);
			y[j] += item;
		}
	}
	// ���������� ����������-������� �������. O(lines*columns)
	for (size_t i = 0; i < v->lines; ++i) {
		for (size_t j = 0; j < v->columns; ++j) {
			double v_ij = lpsa_matrix_get(v, i, j);
			double p_ij = x[i] * y[j] / (N*N) + 0.00001;
			lpsa_matrix_set(m, i, j,
				(v_ij - N * p_ij)
				/
				sqrt(N*p_ij*(1 - p_ij))
			);
		}
	}
	free(x);
	free(y);
	return NULL;
	// ���������: O(lines*columns)
}

// �������� ��������� ��������� ������
lpsa_error* create_matrix_set(
	const lpsa_model*             model, 
	const lpsa_symbolic_sequence* seq,
	const lpsa_symbolic_sequence* artificial,
	OUT lpsa_matrix**             set,
	OUT lpsa_symbolic_sequence**  randoms
) {
	// �������������. O(1)
	lpsa_matrix* sfreq = lpsa_matrix_init(seq->count_symbols, model->period_length);
	create_frequency_matrix(artificial, seq, sfreq);
	lpsa_matrix* sweight = lpsa_matrix_init(sfreq->lines, sfreq->columns);
	create_weight_matrix(sfreq, sweight);
	lpsa_matrix* snorm = lpsa_matrix_init(sweight->lines, sweight->columns);
	calculate_m_prime(sweight, seq, sqrt(model->R_sqr), model->K_d, model->period_length, snorm);
	size_t set_count = 1;
	// � �������� ��������� ������� �� ��������� ������������ ������� ����������� ������������������.
	// ��� ������� ��� ����, ����� �� �������� �� ������� ������� ������� � ����������.
	set[0] = snorm;
	// ������� ���� ����� ������� �� �����������, ���� �������� ������������ ���������� ����� ���������
	// ����� ������� �������
	for (;;) {
		if (set_count == model->size_of_matrix_set) {
			break;
		}
		// �������� ��������� ������������������
		lpsa_symbolic_sequence* random = generate_random_sequence_from_existed(seq);
		// Ÿ ��������� �������
		lpsa_matrix* freq = lpsa_matrix_init(seq->count_symbols, model->period_length);
		lpsa_error* err = create_frequency_matrix(artificial, random, freq);
		if (err != NULL) {
			return lpsa_error_wrap(err, LPSA_WRAPPED_ERROR_TYPE, "problems in creating frequency matrix");
		}
		// Ÿ ����������-������� �������
		lpsa_matrix* weight = lpsa_matrix_init(freq->lines, freq->columns);
		err = create_weight_matrix(freq, weight);
		if (err != NULL) {
			return lpsa_error_wrap(err, LPSA_WRAPPED_ERROR_TYPE, "problems in creating weight matrix");
		}
		// Ÿ M' �������
		lpsa_matrix* norm = lpsa_matrix_init(weight->lines, weight->columns);
		calculate_m_prime(weight, random, sqrt(model->R_sqr), model->K_d, model->period_length, norm);
		char is_added = 0;
		// �������� ���������� �� ���� ������ �� ���������
		for (size_t i = 0; i < set_count; ++i) {
			double* dist = calloc(sizeof(double), 1);
			err = lpsa_matrix_distance(norm, set[i], dist);
			if (err != NULL) {
				return lpsa_error_wrap(err, LPSA_WRAPPED_ERROR_TYPE, "problem in calculating distance while creating a matrix set");
			}
			if (*dist < model->min_distance_between_matrices) {
				printf("[*] Set count = %d\r", set_count);
				is_added = 1;
				break;
			}
		}
		if (is_added == 1) {
			continue;
		}
		// ���������� �� ���������
		set[set_count] = norm;
		randoms[set_count] = random;
		set_count++;
		lpsa_matrix_free(freq);
		lpsa_matrix_free(weight);
	}
	// ������ ������� ����������� ������������������ �� ���������
	lpsa_symbolic_sequence* random = generate_random_sequence_from_existed(seq);
	lpsa_matrix* rfreq = lpsa_matrix_init(seq->count_symbols, model->period_length);
	create_frequency_matrix(artificial, random, rfreq);
	lpsa_matrix* rweight = lpsa_matrix_init(rfreq->lines, rfreq->columns);
	create_weight_matrix(rfreq, rweight);
	lpsa_matrix* rnorm = lpsa_matrix_init(rweight->lines, rweight->columns);
	calculate_m_prime(rweight, random, sqrt(model->R_sqr), model->K_d, model->period_length, rnorm);
	set[0] = rnorm;
	randoms[0] = random;
	return NULL;
}


int* calculate_b(const lpsa_symbolic_sequence* S){
	int* b = (int *)calloc(S->count_symbols, sizeof(int));
	for (size_t i = 0; i < S->length; ++i){
		b[S->symbols[i]]++;
	}
	return b;
}

double calculate_Kd(const lpsa_symbolic_sequence* S, const lpsa_matrix* M, const lpsa_model* model){
	double K = 0;
	int* b = calculate_b(S);
	for (size_t i = 0; i < M->lines; i++){
		for (size_t j = 0; j < M->columns; j++){
			double m_ij = lpsa_matrix_get(M, i, j);
			double b_i = b[i];
			double N = S->length;
			double n = model->period_length;
			////printf("Mij = %lf, Bi = %d, Slength = %ld, P = %ld\n\r", lpsa_matrix_get(M, i, j), b[i], S->length, model->period_length);
			K += m_ij * (b_i / N)*(1 / n);
			////printf("K = %lf\n\r", K);
		}
	}
	free(b);
	return K;
}

double calculate_R_sq(const lpsa_matrix* M){
	double res = 0;
	for (size_t i = 0; i < M->lines; i++){
		for (size_t j = 0; j < M->columns; j++){
			res += pow(lpsa_matrix_get(M, i, j),2) ;
		}
	}
	return res;
}

// ���������� ������� M'
lpsa_error* calculate_m_prime(
	const lpsa_matrix*            m, 
	const lpsa_symbolic_sequence* S, 
	const double                  R, 
	const double                  K_d, 
	const size_t                  period_length, 
	OUT lpsa_matrix*              m_prime) 
{
	// ������������� ����� p[i,j]. ��� ��� ����� S � ����� �������. O(n) + O(lines*columns)
	size_t* b_arr = calloc(sizeof(size_t), S->count_symbols);
	for (size_t i = 0; i < S->length; ++i) {
		b_arr[S->symbols[i]]++;
	}
	//size_t sum = 0;
	//for (size_t i = 0; i < S->count_symbols; ++i) {
	//	printf("%d ", b_arr[i]);
	//	sum += b_arr[i];
	//}
	//printf("\nsum=%d\n", sum);
	lpsa_matrix* p = lpsa_matrix_init(S->count_symbols, period_length);
	for (size_t i = 0; i < p->lines; ++i) {
		for (size_t j = 0; j < p->columns; ++j) {
			lpsa_matrix_set(p, i, j,
				(1.0 / (double)period_length) * ((double)b_arr[i] / (double)S->length)
			);
		}
	}
	free(b_arr);
	// ���������� ����������� ���� ��� ���������� ����������. O(lines*columns)
	double sum_p_2 = 0;
	double sum_p_m = 0;
	for (size_t i = 0; i < p->lines; ++i) {
		for (size_t j = 0; j < p->columns; ++j) {
			double p_ij = lpsa_matrix_get(p, i, j);
			double m_ij = lpsa_matrix_get(m, i, j);
			sum_p_m += p_ij * m_ij;
			sum_p_2 += p_ij * p_ij;
		}
	}
	//printf("sum_p_2=%f\n", sum_p_2);
	//printf("sum_p_m=%f\n", sum_p_m);
	double a = (K_d - sum_p_m) / sum_p_2;
	double b = K_d / sum_p_2;
	//printf("a=%f\n", a);
	//printf("b=%f\n", b);
	// ��� ���� ����������� �����. O(lines*columns)
	double sum_m_p_a_b = 0;
	for (size_t i = 0; i < p->lines; ++i) {
		for (size_t j = 0; j < p->columns; ++j) {
			double p_ij = lpsa_matrix_get(p, i, j);
			double m_ij = lpsa_matrix_get(m, i, j);
			double temp = m_ij + p_ij * (a - b);
			temp *= temp;
			sum_m_p_a_b += temp;
		}
	}
	//printf("sum_m_p_a_b=%f\n", sum_m_p_a_b);
	// ��������� ���������� ���������� ��������� `t`. O(1)
	double B = (2 * b*(sum_p_m + (a - b)*sum_p_2)) / sum_m_p_a_b;
	double C = (b*b*sum_p_2 - R * R) / sum_m_p_a_b;
	double D = B * B - 4 * C;
	double t_1 = 0.5*(-B - sqrt(D));
	double t_2 = 0.5*(-B + sqrt(D));
	double t = lpsa_max(t_1, t_2);
	//printf("B=%f\n", B);
	//printf("C=%f\n", C);
	//printf("D=%f\n", D);
	//printf("t_1=%f\n", t_1);
	//printf("t_2=%f\n", t_2);
	//printf("t=%f\n", t);
	// ���������� ������� M'. O(lines*columns)
	lpsa_matrix_set_all(m_prime, 0);
	for (size_t i = 0; i < p->lines; ++i) {
		for (size_t j = 0; j < p->columns; ++j) {
			double p_ij = lpsa_matrix_get(p, i, j);
			double m_ij = lpsa_matrix_get(m, i, j);
			lpsa_matrix_set(m_prime, i, j,
				p_ij*b + t * (m_ij + p_ij * (a - b))
			);
		}
	}
	return NULL;
	// ���������: O(n) + 4*O(lines*columns)
}

/*
���������� ������������ S � S1. 
���� ���������� ������ �������� F_max, ���������� ������� NULL � ��������� S_alignment � S1_alignment.
*/
void align_sequence_with_ideal(
	const lpsa_symbolic_sequence* S,            // ����������� ������������������
	const lpsa_symbolic_sequence* S1,           // ������������� ������������������
	const lpsa_matrix*            M_,           // ������� ������������
	const lpsa_model*             model,        // ����� ���������������
	unsigned char                 align_type,   // ����������/��������� ������������
	OUT lpsa_symbolic_sequence*   S_alignment,  // ��������� ������������ 
	OUT lpsa_symbolic_sequence*   S1_alignment, // ��������� ������������
	OUT double*                   F_max         // ��� ������������
) {
	// ������������� ������ ��� ���������� ������������
	lpsa_matrix* F = lpsa_matrix_init(S->length, S1->length);
	lpsa_matrix* F1 = lpsa_matrix_init(S->length, S1->length);
	lpsa_matrix* F2 = lpsa_matrix_init(S->length, S1->length);
    unsigned char*** F_ = make_uchar_matrix3(S->length, S1->length);
	// ���������� ������ ����������
	fill_matrices(S, S1, F, F1, F2, F_, M_, model, align_type);
	// ���������� �������� F_max 
	if (align_type == LPSA_ALIGN_TYPE_GLOBAL) {
		*F_max = lpsa_matrix_get(F, F->lines - 1, F->columns - 1);
	}
	else {
		lpsa_matrix_max(F, F_max, NULL, NULL);
	}
	size_t* i_max = NULL;
	size_t* j_max = NULL;
	lpsa_symbol* S_alignment_buffer = NULL;
	lpsa_symbol* S1_alignment_buffer = NULL;
	// �������� ������������
	if ((S_alignment != NULL) && (S1_alignment != NULL)) {
		i_max = (size_t*)malloc(sizeof(size_t));
		j_max = (size_t*)malloc(sizeof(size_t));
		if (align_type == LPSA_ALIGN_TYPE_GLOBAL) {
			*i_max = F->lines - 1;
			*j_max = F->columns - 1;
		}
		else {
			lpsa_matrix_max(F, NULL, i_max, j_max);
		}
		size_t i_current = *i_max;
		size_t j_current = *j_max;
		size_t current_state = F_[0][i_current][j_current];
		S_alignment_buffer = (lpsa_symbol*)malloc(sizeof(lpsa_symbol)*S->length*2 + 1);
		S1_alignment_buffer = (lpsa_symbol*)malloc(sizeof(lpsa_symbol)*S1->length*2 + 1);
		size_t align_position = 0;
		while (current_state != 3){  // ���� �� ��������� �������
			// ���� � ���� �������
			if (i_current == 0) {
				S_alignment_buffer[align_position] = 255;
				S1_alignment_buffer[align_position] = S1->symbols[j_current];
				--j_current;
			}
			else if (j_current == 0){
				S_alignment_buffer[align_position] = S->symbols[i_current];
				S1_alignment_buffer[align_position] = 255;
				--i_current;
			}
			else {
				// ������ �������
				if (current_state == 0){
					S_alignment_buffer[align_position] = S->symbols[i_current];
					S1_alignment_buffer[align_position] = S1->symbols[j_current];
					--i_current;
					--j_current;
				}
				else if (current_state == 1) {
					S_alignment_buffer[align_position] = S->symbols[i_current];
					S1_alignment_buffer[align_position] = 255;
					--i_current;
				}
				else if (current_state == 2) {
					S_alignment_buffer[align_position] = 255;
					S1_alignment_buffer[align_position] = S1->symbols[j_current];
					--j_current;
				}
				else {
					break;
				}
			}
			current_state = F_[current_state][i_current][j_current];
			++align_position;
		}
		// ���������� ��������� ������
		S_alignment_buffer[align_position] = S->symbols[i_current];
		S1_alignment_buffer[align_position] = S1->symbols[j_current];
		++align_position;

		S_alignment->symbols = (lpsa_symbol*)malloc(sizeof(lpsa_symbol)*align_position);
		S1_alignment->symbols = (lpsa_symbol*)malloc(sizeof(lpsa_symbol)*align_position);
		for (size_t i = 0; i < align_position; ++i){
			S_alignment->symbols[i] = S_alignment_buffer[align_position - 1 - i];
			S1_alignment->symbols[i] = S1_alignment_buffer[align_position - 1 - i];
		}
		S_alignment->count_symbols = S->count_symbols;
		S1_alignment->count_symbols = S1->count_symbols;
		S_alignment->length = align_position;
		S1_alignment->length = align_position;
	}
	// �������� ������
	lpsa_matrix_free(F);
	lpsa_matrix_free(F1);
	lpsa_matrix_free(F2);
	for (size_t i = 0; i < 3; ++i){
		for (size_t j = 0; j < S->length; ++j){
			free(F_[i][j]);
		}
		free(F_[i]);
	}
	free(F_);
	if ((S_alignment != NULL) && (S1_alignment != NULL)){
		free(i_max);
		free(j_max);
		free(S_alignment_buffer);
		free(S1_alignment_buffer);
	}
}

/*
���������� ���������� ������. � ������� �������� ��������� F_:
	0 ��� F,
	1 ��� F1,
	2 ��� F2,
	3 ����������.
*/
void fill_matrices(
	const lpsa_symbolic_sequence* S, 
	const lpsa_symbolic_sequence* S1, 
	OUT lpsa_matrix*              F,
	OUT lpsa_matrix*              F1,
	OUT lpsa_matrix*              F2,
	OUT unsigned char***          F_, 
	const lpsa_matrix*            M_,
	const lpsa_model*             model,
	unsigned char                 align_type
) {
	// ������ ������ � ������� � �������� ������ ���� ��������� ������ ���� ������������ ���������
	// � ����� ������ ������������� ��������, ���� ������������ ����������
	if (align_type == LPSA_ALIGN_TYPE_GLOBAL){
		double c = -10 * model->d_price;
		size_t pos = 0;
		// ����� ������������ ���� ����, �.�. `F->lines` ����� `F->columns`
		for (size_t i = 1; i < F->lines; ++i){
			pos += F->columns;
			c -= 5 * model->d_price;
			lpsa_matrix_set_fast(F, pos, c);
			lpsa_matrix_set_fast(F1, pos, c);
			lpsa_matrix_set_fast(F2, pos, c);
			lpsa_matrix_set_fast(F, i, c);
			lpsa_matrix_set_fast(F1, i, c);
			lpsa_matrix_set_fast(F2, i, c);
		}
	}
	else {
		// �� ����� ������� F ������ ���� ��������� ����� � ������ ���������� ������������
		for (size_t i = 0; i < F->lines; ++i) {
			for (size_t j = 0; j < F->columns; ++j) {
				F_[0][i][j] = F_[1][i][j] = F_[2][i][j] = 3;
			}
		}
	}
	// ��������� ����� (������� ����� ����)
	F_[0][0][0] = F_[1][0][0] = F_[2][0][0] = 3;
	// ������ ��������� ���������� ������
	for (size_t i = 1; i < F->lines; ++i) { 
		for (size_t j = 1; j < F->columns; ++j) {
			// ��������� ������� F
			double m_ij = lpsa_matrix_get(M_, S->symbols[i], S1->symbols[j]);
			double from_F = lpsa_matrix_get(F, i - 1, j - 1) + m_ij;
			double from_F1 = lpsa_matrix_get(F1, i - 1, j - 1) + m_ij; //- model->d_price; //??? + m_ij;
			double from_F2 = lpsa_matrix_get(F2, i - 1, j - 1) + m_ij; //- model->d_price; //??? + m_ij;
			double maximum = lpsa_max(from_F, lpsa_max(from_F1, from_F2));
			if (align_type == LPSA_ALIGN_TYPE_LOCAL) {
				maximum = lpsa_max(0, maximum);
				if (maximum == 0) {
					F_[0][i][j] = 3; // ����� ��������� �����
				}
			}
			if (maximum == from_F) { 
				F_[0][i][j] = 0; 
			}
			else if (maximum == from_F1) { 
				F_[0][i][j] = 1; 
			}
			else if (maximum == from_F2) { 
				F_[0][i][j] = 2; 
			}
			lpsa_matrix_set(F, i, j, maximum);
			// ��������� ������� F_1
			from_F = lpsa_matrix_get(F, i - 1, j) - model->d_price;
			from_F1 = lpsa_matrix_get(F1, i - 1, j) - model->e_price;
			maximum = lpsa_max(from_F, from_F1);
			if (align_type == LPSA_ALIGN_TYPE_LOCAL) {
				maximum = lpsa_max(0, maximum);
				if (maximum == 0) {
					F_[1][i][j] = 3; // ����� ��������� �����
				}
			}
			if (maximum == from_F) { 
				F_[1][i][j] = 0; 
			}
			else if (maximum == from_F1) {
				F_[1][i][j] = 1; 
			}
			lpsa_matrix_set(F1, i, j, maximum);
			// ��������� ������� F_2
			from_F = lpsa_matrix_get(F, i, j - 1) - model->d_price;
			from_F2 = lpsa_matrix_get(F2, i, j - 1) - model->e_price;
			maximum = lpsa_max(from_F, from_F2);
			if (align_type == LPSA_ALIGN_TYPE_LOCAL) {
				maximum = lpsa_max(0, maximum);
				if (maximum == 0) {
					F_[2][i][j] = 3; // ����� ��������� �����
				}
			}
			if (maximum == from_F) { 
				F_[2][i][j] = 0; 
			}
			else if (maximum == from_F2) { 
				F_[2][i][j] = 2; 
			}
			lpsa_matrix_set(F2, i, j, maximum);
		}
	}
}

double mean_F_max(
	double* F_max, 
	size_t     n)
{
	double res = 0;
	for (size_t i = 0; i < n; ++i){
		res += F_max[i];
	}
	return res / n;
}

double unshifted_dispersion_F_max(
	double* F_max, 
	size_t     n,
	double  mean)
{
	double res = 0;
	for (size_t i = 0; i < n; ++i){
		res += pow(F_max[i] - mean, 2);
	}
	return res / (n - 1);
}

double stat_significance_F_max(
	double* F_max, 
	size_t     n, 
	double  estimated)
{
	double m = mean_F_max(F_max, n);
	//for (size_t i = 0; i < n; ++i) {
	//	printf("F_random=%f\n", F_max[i]);
	//}
	double lmax = F_max[0];
	double lmin = F_max[0];
	for (size_t i = 0; i < n; ++i) {
		if (F_max[i] > lmax) { lmax = F_max[i]; }
		if (F_max[i] < lmin) { lmin = F_max[i]; }
	}
	printf("< Mean=%f, Std=%f, Min=%f, Max=%f\n", m, sqrt(unshifted_dispersion_F_max(F_max, n, m)), lmin, lmax);
	return (estimated - m) / sqrt(unshifted_dispersion_F_max(F_max, n, m));
}


lpsa_error* create_alignment(
	lpsa_model*             model,
	const lpsa_symbolic_sequence* S,
	const lpsa_symbolic_sequence* artif_seq,
	OUT lpsa_symbolic_sequence*   S_alignment,
	OUT lpsa_symbolic_sequence*   S1_alignment,
	OUT double*                   F_max,
	unsigned char				  align_type, // Values: LPSA_ALIGN_TYPE_LOCAL, LPSA_ALIGN_TYPE_GLOBAL
	unsigned char			      rand) // Values: LPSA_MODE_INITIAL, LPSA_MODE_RANDOM
{
	// Initializing sequence for which we will create alignment.
	lpsa_symbolic_sequence* operating_seq;
	if (rand == LPSA_MODE_RANDOM) {
		operating_seq = generate_random_sequence_from_existed(S);
	}
	else {
		operating_seq = S;
	}
	// Creating frequency matrix V.
	lpsa_matrix* freq_matrix = lpsa_matrix_init(operating_seq->count_symbols, artif_seq->count_symbols);
	lpsa_error* err = create_frequency_matrix(artif_seq, operating_seq, freq_matrix);
	if (err != NULL) {
		if (rand == LPSA_MODE_RANDOM) {
			free(operating_seq->symbols);
			free(operating_seq);
		}
		lpsa_matrix_free(freq_matrix);
		return err;
	}
	// Creating weight matrix M using V.
	lpsa_matrix* M = lpsa_matrix_init(freq_matrix->lines, freq_matrix->columns);
	err = create_weight_matrix(freq_matrix, M);
	if (err != NULL) {
		if (rand == LPSA_MODE_RANDOM) {
			free(operating_seq->symbols);
			free(operating_seq);
		}
		lpsa_matrix_free(freq_matrix);
		lpsa_matrix_free(M);
		return err;
	}
	// If initial, setting R^2 and K_d.
	//!
	//!if (rand == LPSA_MODE_INITIAL){
		model->K_d = calculate_Kd(S, M, model);
		//model->K_d = -0.1;
		model->R_sqr = calculate_R_sq(M);
		//double sxi = 1050;
		//model->R_sqr = (sxi / pow(7, 0.61))*(pow(model->period_length, 0.61));
	//!}
	// Calculating M' matrix.
	
	lpsa_matrix* M_ = lpsa_matrix_init(M->lines, M->columns);
	err = calculate_m_prime(M, S, sqrt(model->R_sqr), model->K_d, model->period_length, M_);
	if (err != NULL){
		if (rand){
			free(operating_seq->symbols);
			free(operating_seq);
		}
		lpsa_matrix_free(freq_matrix);
		lpsa_matrix_free(M);
		lpsa_matrix_free(M_);
		return err;
	}
	
	// Do alignment with artifitial sequence using M' matrix.
	align_sequence_with_ideal(S, artif_seq, M_, model, align_type, S_alignment, S1_alignment, F_max);
	// Free memory.
	lpsa_matrix_free(M);
	lpsa_matrix_free(M_);
	lpsa_matrix_free(freq_matrix);
	if (rand){
		free(operating_seq->symbols);
		free(operating_seq);
	}
	return NULL;
}

lpsa_error* lpsa_algorithm(
	const lpsa_model*             model,
	const lpsa_symbolic_sequence* S,
	OUT lpsa_symbolic_sequence*   S_alignment, 
	OUT lpsa_symbolic_sequence*   S1_alignment, 
	OUT double*                   F_max,
	OUT double*					  significance,
	unsigned char				  alignment_type) //bool (constants LPSA_ALIGN_TYPE_GLOBAL/LPSA_ALIGN_TYPE_LOCAL)
{
	lpsa_error* err = NULL;
	lpsa_symbolic_sequence* artif_seq = create_artificial_sequence(model, S->length);
	double* F_array = (double*) malloc(sizeof(double)*model->size_of_matrix_set);

	err = create_alignment(model, S, artif_seq, S_alignment, S1_alignment, F_max, alignment_type, LPSA_MODE_INITIAL);
	if (err != NULL){
		free(artif_seq->symbols);
		free(artif_seq);
		free(F_array);
		return err;
	}

	for (size_t i = 0; i < model->size_of_matrix_set; ++i){
		err = create_alignment(model, S, artif_seq, S_alignment, S1_alignment, F_array + i, alignment_type, LPSA_MODE_RANDOM);
		if (err != NULL){
			free(artif_seq->symbols);
			free(artif_seq);
			free(F_array);
			return err;
		}
	}

	*significance = stat_significance_F_max(F_array, model->size_of_matrix_set, *F_max);

	free(F_array);
	free(artif_seq->symbols);
	free(artif_seq);
	return NULL;
}

void* lpsa_algorithm_async(void* value) {
	lpsa_task_args* args = (lpsa_task_args*)value;
	lpsa_algorithm_args* largs = (lpsa_algorithm_args*)args->args;
	time_t* elapsed = malloc(sizeof(time_t));
	*elapsed = time(NULL);
	lpsa_error* err = lpsa_algorithm(
		largs->model,
		largs->S,
		largs->S_alignment,
		largs->S1_alignment,
		largs->F_max,
		largs->significance,
		largs->alignment_type
	);
	*elapsed = time(NULL) - *elapsed;
	lpsa_algorithm_result* result = calloc(sizeof(lpsa_algorithm_result), 1);
	result->F_max = largs->F_max;
	result->significance = largs->significance;
	result->period = largs->model->period_length;
	result->elapsed_time = elapsed;
	lpsa_conc_query_push(args->container, result);
	return NULL;
}

void* create_alignment_async(void* value){
	lpsa_task_args* args = (lpsa_task_args*)value;
	create_alignment_args* largs = (create_alignment_args*)args->args;
	lpsa_error* err = create_alignment(
		largs->model,
		largs->S,
		largs->artif_seq,
		largs->S_alignment,
		largs->S1_alignment,
		largs->F_max,
		largs->global,
		largs->rand
	);
	if (args->container == NULL || args->container->count == -1){
		free(largs);
		free(args);
		free(err);
		return NULL;
	}
	lpsa_conc_query_push(args->container, err);
	free(largs);
	free(args);
	return NULL;
}


lpsa_error* lpsa_algorithm_multiple_threads(
	const lpsa_model*             model,
	const lpsa_symbolic_sequence* S,
	OUT lpsa_symbolic_sequence*   S_alignment, 
	OUT lpsa_symbolic_sequence*   S1_alignment, 
	OUT double*                   F_max,
	OUT double*					  significance,
	unsigned char				  alignment_type, //bool (constants GLOBAL/LOCAL)
	lpsa_thread_pool*             pool)
{
	lpsa_conc_query* query = lpsa_conc_query_init();
	lpsa_functor* functor = calloc(sizeof(lpsa_functor), 1);
	*functor = create_alignment_async;

	lpsa_error* err = NULL;
	double* F_array = (double*) malloc(sizeof(double)*model->size_of_matrix_set);
	lpsa_symbolic_sequence* artif_seq = create_artificial_sequence(model, S->length);

	err = create_alignment(model, S, artif_seq, S_alignment, S1_alignment, F_max, alignment_type, LPSA_MODE_INITIAL);
	if (err != NULL){
		lpsa_conc_query_free(query);
		free(artif_seq->symbols);
		free(artif_seq);
		free(F_array);
		free(functor);
		free(query);
		return err;
	}

	for (size_t i = 0; i < model->size_of_matrix_set; ++i){
		create_alignment_args* args = calloc(sizeof(create_alignment_args), 1);
		args->artif_seq = artif_seq;
		args->F_max = F_array + i;
		args->global = alignment_type;
		args->model = model;
		args->rand = LPSA_MODE_RANDOM;
		args->S1_alignment = NULL;
		args->S = S;
		args->S_alignment = NULL;

		lpsa_task_args* task_args = calloc(sizeof(lpsa_task_args), 1);
		task_args->args = args;
		task_args->container = query;
		lpsa_task* task = calloc(sizeof(lpsa_task), 1);
		task->function = functor;
		task->arg = task_args;
		lpsa_thread_pool_add(pool, task);
	}

	size_t results_count = 0;
	while (results_count != model->size_of_matrix_set) {
		if (query->count != 0) {
			err = (lpsa_error*)lpsa_conc_query_pop(query);
			if (err != NULL){
				lpsa_conc_query_free(query);
				free(artif_seq->symbols);
				free(artif_seq);
				free(F_array);
				free(functor);
				free(query);
				return err;
			}
			results_count++;
		}
		wait;
	}

	*significance = stat_significance_F_max(F_array, model->size_of_matrix_set, *F_max);

	lpsa_conc_query_free(query);
	free(artif_seq->symbols);
	free(artif_seq);
	free(F_array);
	free(functor);
	free(query);
	return NULL;
}
