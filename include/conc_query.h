#pragma once

#include <stdlib.h>
#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>

#if defined(_WIN32)
#define wait _sleep(10)
#endif

#if defined(linux)
#define wait __asm__ volatile( \
				"nop \n\r" \
			)
#endif

// ���� ������
typedef struct {
	void* value; // �������� ������������� ����
	void* next;  // ��������� �� ��������� ����
} lpsa_node;

// ������������ �������
typedef struct {
	lpsa_node*       head;  // ��������� �� ������
	lpsa_node*       tail;  // ��������� �� �����
	size_t           count; // ���������� ���������
	pthread_mutex_t* mutex; // �������
} lpsa_conc_query;

// ����������� �������
lpsa_conc_query* lpsa_conc_query_init();

// ���������� �������� � ����� �������
void lpsa_conc_query_push(lpsa_conc_query* query, void* value);

// ������ �������� �� ������ �������
void* lpsa_conc_query_pop(lpsa_conc_query* query);

// ������������ ������ ��� �������
void lpsa_conc_query_free(lpsa_conc_query* query);

// �������� ���������� � ������� `limit` ���������
void lpsa_conc_query_wait(lpsa_conc_query* query, size_t limit);