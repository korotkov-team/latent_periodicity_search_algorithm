#pragma once

#include <stdlib.h>
#include <time.h>
#include "model.h"
#include "lpsa_symb_seq.h"
#include "error.h"
#include "matrix.h"
#include "thread_pool.h"

#define LPSA_ALIGN_TYPE_GLOBAL 1
#define LPSA_ALIGN_TYPE_LOCAL 0
#define LPSA_MODE_RANDOM 1
#define LPSA_MODE_INITIAL 0
#define MAXSIZET 4294967295

typedef struct {
	lpsa_model*             	  model;
	lpsa_symbolic_sequence*       S;
	OUT lpsa_symbolic_sequence*   S_alignment;
	OUT lpsa_symbolic_sequence*   S1_alignment;
	OUT double*                   F_max;
	OUT double*					  significance;
	unsigned char				  alignment_type; //bool (constants LPSA_ALIGN_TYPE_GLOBAL/LPSA_ALIGN_TYPE_LOCAL)
} lpsa_algorithm_args;

typedef struct {  
	lpsa_model*                   model;
	lpsa_symbolic_sequence*       S;
	lpsa_symbolic_sequence*       artif_seq;
	OUT lpsa_symbolic_sequence*   S_alignment;
	OUT lpsa_symbolic_sequence*   S1_alignment;
	OUT double*                   F_max;
	unsigned char				  global; //bool
	unsigned char			      rand; //bool
} create_alignment_args;

/*
Randomly permutes symbols of `seq` and return new sequence.
*/
lpsa_symbolic_sequence* generate_random_sequence_from_existed(
	const lpsa_symbolic_sequence* seq);

/*
Randomly permutes parts of `seq` and return a new sequence. Parameter `block` is length of part.
*/
lpsa_symbolic_sequence* generate_random_sequence_from_existed_blocks(
	const lpsa_symbolic_sequence* seq, 
	const size_t                  block);

/*
Creates sequence of type [1, ... n, 1, ... n, ... ].
*/
lpsa_symbolic_sequence* create_artificial_sequence(
	const lpsa_model* model, 
	size_t            seq_length);

/*
Fill `out` matrix with frequences of elements in `random` sequence compared to `artificial`.
Matrix `out` must be allocated before using this function.
*/
lpsa_error* create_frequency_matrix(
	const lpsa_symbolic_sequence* artificial, 
	const lpsa_symbolic_sequence* random, 
	OUT lpsa_matrix*               V);

/*
Matrix `v` is frequency matrix. Matrix `m` is weight matrix. Designations are chosen to match the designations from the article.
Matrix `m` must be allocated.
*/
lpsa_error* create_weight_matrix(
	const lpsa_matrix* v, 
	OUT lpsa_matrix*   m);

int* calculate_b(const lpsa_symbolic_sequence* S);

double calculate_Kd(const lpsa_symbolic_sequence* S, const lpsa_matrix* M, const lpsa_model* model);

double calculate_R_sq(const lpsa_matrix* M);

/*
Matrix M' is m_prime.
*/
lpsa_error* calculate_m_prime(
	const lpsa_matrix*             m, 
	const lpsa_symbolic_sequence* S, 
	const double                  R, 
	const double                  K_d, 
	const size_t                  period_length, 
	OUT lpsa_matrix*               m_prime);

/*
Creates set of random position-weight matrices.
*/
lpsa_error* create_matrix_set(
	const lpsa_model*             model, 
	const lpsa_symbolic_sequence* seq, 
	const lpsa_symbolic_sequence* artificial, 
	OUT lpsa_matrix**             set,
	OUT lpsa_symbolic_sequence**  randoms
);

void align_sequence_with_ideal(
	const lpsa_symbolic_sequence* S, 
	const lpsa_symbolic_sequence* S1, 
	const lpsa_matrix*             Mq, 
	const lpsa_model*             model, 
	unsigned char                 global, //bool
	OUT lpsa_symbolic_sequence*   S_alignment, 
	OUT lpsa_symbolic_sequence*   S1_alignment, 
	OUT double*                   F_max);


void fill_matrices(
	const lpsa_symbolic_sequence* S, 
	const lpsa_symbolic_sequence* S1, 
	OUT lpsa_matrix*               F, 
	OUT lpsa_matrix*               F1, 
	OUT lpsa_matrix*               F2, 
	OUT unsigned char***          F_, 
	const lpsa_matrix*             Mq, 
	const lpsa_model*             model,
	unsigned char                 global); //bool


double mean_F_max(double* F_max, size_t n);

double unshifted_dispersion_F_max(double* F_max, size_t n, double mean);

double stat_significance_F_max(double* F_max, size_t n, double estimated);

lpsa_error* create_alignment(
	lpsa_model*                   model,
	const lpsa_symbolic_sequence* S,
	const lpsa_symbolic_sequence* artif_seq,
	OUT lpsa_symbolic_sequence*   S_alignment,
	OUT lpsa_symbolic_sequence*   S1_alignment,
	OUT double*                   F_max,
	unsigned char				  global, //bool
	unsigned char			      rand); // bool

lpsa_error* lpsa_algorithm(
	const lpsa_model*             model,
	const lpsa_symbolic_sequence* S,
	OUT lpsa_symbolic_sequence*   S_alignment,
	OUT lpsa_symbolic_sequence*   S1_alignment,
	OUT double*                   F_max,
	OUT double*					  significance,
	unsigned char				  alignment_type); //bool (constants LPSA_ALIGN_TYPE_GLOBAL/LPSA_ALIGN_TYPE_LOCAL)

typedef struct {
	size_t                  period;
	time_t*                 elapsed_time;
	lpsa_symbolic_sequence* S_alignment;
	lpsa_symbolic_sequence* S1_alignment;
	double*                 F_max;
	double*					significance;
} lpsa_algorithm_result;

void* lpsa_algorithm_async(void* value);

typedef struct {
	//size_t                  index; //for defined sequence = MAXSIZET
	lpsa_error*             err;
} create_alignment_result;

void* create_alignment_async(void* value);

lpsa_error* lpsa_algorithm_multiple_threads(
	const lpsa_model*             model,
	const lpsa_symbolic_sequence* S,
	OUT lpsa_symbolic_sequence*   S_alignment, 
	OUT lpsa_symbolic_sequence*   S1_alignment, 
	OUT double*                   F_max,
	OUT double*					  significance,
	unsigned char				  alignment_type,//bool (constants GLOBAL/LOCAL)
	lpsa_thread_pool*             pool);

lpsa_error* lpsa_start_with_normalization(
	const lpsa_model* model,
	const lpsa_symbolic_sequence* sequence,
	OUT lpsa_symbolic_sequence*   aligned_sequence,
	OUT lpsa_symbolic_sequence*   aligned_artificial,
	OUT double*					  significance
);