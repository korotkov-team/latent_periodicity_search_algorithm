# Latent Periodicity Search Algorithm

## Dependences

This project requires: 
* GNU Scientific Library (GSL) v2.2
    * Official site: [link](http://www.gnu.org/software/gsl/)
    * Windows binaries: [link](https://www.bruot.org/hp/libraries/)

## Config

File `config.txt` contains an example of configuration with default values.