#### `lpsa_symbolic_sequence`
```c
typedef struct {
    size_t       count_symbols;
    size_t       length;
    lpsa_symbol* symbols;
} lpsa_symbolic_sequence;
```
Представляет собой последовательность символов.  
`count_symbols` - количество различных символов в последовательности  
`length` - количество символов последовательности  
`symbols` - массив, содержащий непосредственно символы